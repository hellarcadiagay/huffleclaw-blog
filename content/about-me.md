---
title: "About Me"
date: 2018-06-20T20:01:07+02:00
menu: "main"
meta: "false"
---

Ann is:

  * geeky
  * poly
  * queer
  * a huffleclaw

She loves:

  * Front-End-Developement
  * iOS
  * Console Hacking
  * Tattoos

She is not:

  * yours.

As of now, you can:

  * Contact her.
  * Book her for workshops.

As of now, you cannot:

  * hire her. #sorrynotsorry
