---
title: "Concept: Content/Trigger Warnings as a Chrome/Firefox Extension"
date: "2018-01-16T19:30:23+02:00"
layout: post
tags: ['feminism', 'web']
---
Since I haven't found a decent content/trigger notification plugin out there for major browsers, I started writing down some thoughts about what a content notification plugin should do, how it should behave, and what the most efficient way is to develop such a plugin for web-browsers. Whether if one is triggered by traumatic words or names, or one just wants to get sure that they'll don't get spoilered about their favorite video games or series, a trigger/content notification plugin may be handy in achieving so, making the everyday web use a more safe and healthy experience. In the following notes I'll try to narrow down what I would suggest in terms of technical implementation of such warnings, and what features I wish that I'm able to code soon.

**Technical Aspects:**

* We need arrays:
  * List of triggering tags and words should be defined first, to add them into (multiple) arrays.</li>
  * Another array should be defined including parent nodes which should be overlayed/hidden (it isn't sufficient to hide the word itself, since a given context can also be triggering), e.g. p, b, div et cetera.
* And some JavaScript magic probably:
  * The DOM should be scanned recursively for words contained in the first mentioned arrays.
 	* If a word on a page matches the first array the plugins compares the second array to the parentNodes element and if it's in the array overlays it with the help of some CSS3 magic.</li>
* What the overlay should contain:
  * The name of category containing the triggering word, so one knows why it is hidden.
  * A "show" button to show the content ignoring the overlay.</li>
* Other things we may consider:
  * The possibility of grouping multiple words into a main category, e.g. "Chloe Price", "Maxine Caulfield", "Rachel Amber" as the category "Life is Strange (Spoiler Warning)" or "anxiety", "panic attack", "social anxiety disorder" as the category "Mental Health Words".
  * The ability to hide and show certain tags easily.
  * Import/Export of .json lists containing predefined lists of content notifications/trigger warnings.

I already created a git repository where I'll upload the source code once I've enough spoons to actually start developing this, you may find it here: [annfables@github](https://github.com/annfables/chrome-content-notes/annfables/chrome-content-notes).
