---
author: ann
title: "tmux"
date: "2018-04-26T19:30:23+02:00"
layout: post
tags: ['linux']
---
Tmux is a terminal multiplexer, allowing you to detach or attach multiple sessions, which come in handy, when either you're running applications you want to come back to every once in a while via ssh, like weechat, or you're having fun with the uneven odds (Sleeping at Last pun intended) of generating Diffie Hellman parameters, what can, depending on the bit size the prime numbers should have, take a really long time (meaning: more than a ~I'm just going to grab a coffee and come back when it finished~ long time). Chances are, that, if your ssh-connection drops, the build-dh process crashes as well, so you'll probably want to use a tmux session for this, you can simply detach, and come back to it later. Other benefits of using tmux are, that you'll be able to split-screen & name different terminal sessions, have multiple paste buffer, resize windows and all of this keyboard controled.

### Install tmux

Tmux should be available in many of the major distributions repositories and installable via package manager.

#### pacman:
{{< highlight shell >}}
pacman -S tmux or tmux-bash-completion (if you want to install the tmux package including auto-completion)
{{< /highlight >}}

#### brew:
{{< highlight shell >}}
brew install tmux (if you're using brew)
{{< /highlight >}}

*Annotation: brew can be easily installed via:*
{{< highlight shell >}}
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
{{< /highlight >}}

### how to use tmux?

#### minds and configs being a mess!

By default tmux loads a config file from the etc directory of the system you're using, wether that can be /etc/, /usr/local/etc or something entirely different, it usually helps giving the man page a look for locating the config file. You may want to mess around with the keymapping at least, since the prefix keys and splitting commands tend to be quite awkward in the first place.

### useful resources

There are plenty of tmux sheets available online focussing different aspects, just do some research yourself, since I don't know what your requierements are & other people already explained how to use the basic functions as well.

Since I found plenty of documentation not covering how to end all tmux processes, here's how:
{{< highlight shell >}}
tmux ls | grep : | cut -d. -f1 | awk '{print substr($1, 0, length($1)-1)}' | xargs kill
{{< /highlight >}}
