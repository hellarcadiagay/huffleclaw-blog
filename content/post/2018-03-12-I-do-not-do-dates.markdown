---
title: "Roses are red, violets are blue, I don't wanna date; probably that is confusing for you"
date: "2018-03-12T19:30:23+02:00"
layout: post
tags: ['poly']
---
Concepts like dates, furthermore romantic relationships as a category in general, are really confusing for me, since I don't like to label my relationships with terms like romantic, platonic, whatever-relationship, since those labels are always deeply linked with generalised expectations people have and I tend to be more in favour of matching individual needs than cultural or societal expectations what a relationship should be. As a conclusion to this, I don't date people, but instead, I choose to have meaningful connections with everyone I choose to share my life with.

The circumstance that I do not date does not mean that I lack of romantic attraction for people nor does it mean that I choose not to love anyone, since I as well love people and I have romantic attractions to some of them. According to Wikipedia, a date is:

> * a form of courtship involving social activity, with the aim of assessing a potential partner
> * the fruit of the date palm (Phoenix dactylifera)
> * a day on a calendar
> * a representation term or class associated with a data element

And while I use or like the last three mentioned objects in this list, the first one, a social activity with the aim of assesing a potential partner isn't my cup of tea at all. This doesn't neccessarly mean that I don't like things like ordering take-away food and spending evenings involving netflix, cuddling and kissing, that I don't want to ironically play mini golf with you after we went to art galleries mocking all those classical art pictures while loving the modernist ones or that awkwardly dancing together all night long to trash-pop tunes doesn't suit me at all, but that I won't attribute all those activities to romantic relationships and while I'm totally in favour of doing all of this, please just don't call it a date, because we're not on our ways trying to fit a cultural concept what relationships have to look like but on our way of just spending some quality time together loving each and every other involved, discussing our boundries and how our interpersonal relationship should look like according to each needs.

If I decide opening up to you, which is a pretty big deal for introvert wallflowers like me, chances are, that I do want you, if you would like to as well, to be a part of my life and that I'm comfortable connecting with you in a longterm. If we decide to kiss, cuddle, doing kink stuff together, reading books to each other and singing cheesy songs at midnight, that's perfectly fine and something I'd like, if we're agreeing on some of that, that's fine too, even none of it is fine since we'll probably find other ways of spending time together we feel comfortable with.

But just keep in mind, that I do not want to compare all the relationships I have with each other and that I appreciate every form of affection individually, since nobody is entitled to replace the love of someone else or whatever a heteronormative society is trying to tell us to. In summary, everything we share together and every time spend together is voluntary, we do so by our own choice, because we want to do that not because we're obligated to.
