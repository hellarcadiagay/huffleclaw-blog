---
title: "Normalize asking for pronouns in web/app developement!"
date: "2018-03-31T19:30:23+02:00"
layout: post
tags: ['web']
---
When registering for most web services and apps, chances are, that people are requested to "select" a gender identity, most time ranging between the binary "female and male" options, sometimes there may be a third option to select too, and if the developers are more aware of gender identities, it may be possible to select from a range of n gender identities. Since gender is a spectrum and something completely individual, all of the approaches trying to depict identities as a list/array will fail. It just isn't possible without erasing identities and discriminating against people who're defining themselves outside of a gender binary or at least outside of the gender identities you've heard of (plot twist: there are more than you probably know).

### gender is a universe

If you're about to say that this means, that the only way to do this thing right is to provide a  text box where people can enter whatever they're identifying as, you're totally right. But first you've to ask yourself a few things first:

1. **Is asking for gender identity in my software necessary to use it?**

I've seen a bunch of services and apps asking for gender identities where I just don't get why I've to give away this information. Why does Spotify need to know that I'm a girl? Why does Facebook ask for my gender identity while Twitter certainly does not? I know there may be legal boundaries when it comes to online shopping or accounting, but for something as simple as listening to music it is completely unneccesary.

2. **Do I want to know the gender identity or just the pronouns?**

While it is pretty easy to just use neutral pronouns in english software, there are languages where neutral pronouns aren't that common and where it may contribute to the user experience when certain pronouns are being used. If you just want to personalise the user experience in that way you do not need to ask for the gender identity but for pronouns and no, identifying as a girl doesn't automatically lead to the use of she/her pronouns as less as non-binary pals automatically go by they/them. Pronouns aren't equal to gender identity.

### how to solve this: worst and best practices

1. **Worst Practice: gender prediction APIs**

So let's say you're in your late 20s, flannel shirts are, beside 90s sitcoms and craft beer of course, your guilty pleasure and you're working in some hipsterish start-up in East-London or Friedrichshain tampering around with cluttered node.js software using twice as much RAM as most of the 2009ish video games may've used, you're identifying yourself as somewaht socially progressive, so you and your company decided to ditch the gender query during the registration process using a fancy-fancy glitterish gender prediction API instead. There are plenty available, like genderize.io, and they all work similar matching names and regions calculating a probability of what gender a person may have, so ```curl https://api.genderize.io/?name=ann&country_id=nl``` results in ```{"name":"ann","gender":"female","probability":0.99,"count":1818}```. This is one of the worst approaches to this problem. Just don't use this. With names like Kim in mind and the awareness that names doesn't say a thing about gender identity and pronouns, it would be a horrible approach letting such an API predicting pronouns.

2. **Okayish Practice: use neutral pronouns and don't ask for the gender identity**

This will float most of the software developements boat since most time, you really do not need a single clue about your users gender, it doesn't matter for streaming audio, nor does it matter for uploading cheesy photos on Instagram. To not exclude anyone and sicne you're probably a nice person (at least you've read this blogpost to this point) you'll just use neutral pronouns instead of a generic masculine.

3. **Best Practice: ask for Pronouns**

If you're able to create an array of gender identities it would be easy to just ask for pronouns and store them in your database instead of various gender identities as well. If you're super lazy and normative you can even (though I disapprove this) give some examples of pronouns people usually use like "she/her"/"he/his"/"they/them", but the better practice is, just asking in a text box for all the pronouns you may want to know, just like: ```xe, xem, xe, xyr, xyrs, xe, xemself```. If you need more examples on this, just check out: [pronoun.is/xe](https://pronoun.is/xe).

It's not that hard, just do something like this:
```
<p>
    <label>Preferred Pronouns</label>
    <input type = "text"
     id = "userPronouns"
     value = "subject-pronoun/object-pronoun/possessive-determiner/possessive-pronoun/reflexive"/>
</p>
```
on your registration page, and it's easier as implementing a large number of gender identities as an array, which is rather a sign of good intentions than of a good solution, since it's still discriminating and wrong, while implementing a free text field won't meet what you probably need anyway.
