---
author: ann
title: "Review | I Love This Part (Tillie Walden)"
date: 2018-06-26T21:43:17+02:00
category: ['books']
---

## information:
* Genre: Graphic Novel
* 68 pages
* 16.99 USD
* published by Avery Hill Publishing (2nd Edition in 2017)
* [more information](https://averyhillpublishing.bigcartel.com/product/i-love-this-part-by-tillie-walden-hardback-edition)

## how I found this book

Today was probably one of those days best described by hot chocolate, books, sad acoustic song playlists in combination with somehow trying to deal with ones own broken heart & a break-up. So eventually one of my significant others broke up with me via text after we haven't seen each other since pride day and haven't texted for a while, so I went to the biggest bookstore I could find, and bought some new books, so I could have things I love looking forward to, instead of having to deal with what became past and my very own broken heart.

## where I read this book

Since I love getting lost in big book stores, I started reading immediately seating myself on one of the benches with a view on numerous bookshelfes on the left and the everyday as usual hassle of one of the major shopping streets on the right. Since it's only a smallish quick-read, even smaller than similar graphic novels like Blue is the Warmest Color, I finished within an hour I guess, and eventually I spend another half crying since the book is as relatable as beautifully drawn.

## what is it about?

**major spoiler warning**
The plot is described on the books spine as "a story of a small love that can make you feel like the biggest thing around" and that foreshadows a lot of what the book's about: two teenage girls out in the nowhere, in between the small towns and small minds of a rural area somewhere in the USA, sharing songs, dreams and mixtapes at first, remaining in heartache & memories they haven't made yet (bonus points if you get the reference!) in the very end of the story.
In between homework, Sims and time spend together in front of a laptop warming each other (and probably their feet being warmed by the power supply of their laptop), they develope a emotionally intense relationship and eventually started loving each other, seeing their relationship split into pieces because of the internalised homophobia of one of the protagonists in the midst of it. And though I really love how Tillie Walden approaches all of this, I kind of have a love-hate-relationship with queer books where the pairing either breaks up because of experienced homophobia, one of the protagonist eventually dies (looking at you Blue is the Warmest Color and of course, depending on your choices, Life is Strange), since we totally need more queer happy endings, whilst there barely are any in most stories. 

Since it's a graphic novel, I have to add, that I'm sooo darn in love with Tillie Waldens drawing style, the panels composition, all wrapped up in either shades of pastel or grayscales, are beautifully tumblr (one can use that as an adjective, right?).

## for what situation is this a good read?

Even if it's such a short read I would recommend not reading it at public places, during train rides, under a uni desk or in between coffee, bagles & commuting to work, but rather when you're at a quite and cozy place where you feel comfortable crying and emotionally accessible for such a beautiful graphic novel. Oh and I wouldn't recommend it reading with a broken queer heart shortly after a break-up either, I can only speak for myself, but it really touched my heart and was way too relatable, remembering own stories and daydreaming about each and every panel.

## do I recommend this book?

Yes, if you're into queer graphic novel, especially if you grew up in a smallish town like I did, you'll probably fancy this book, since it probably feels relatable sharing a similar background and smiliar experiences as described in Tillie Waldens graphic novel.
