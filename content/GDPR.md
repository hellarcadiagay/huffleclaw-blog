---
title: "GDPR"
date: 2018-06-20T19:57:15+02:00
menu: "main"
meta: "false"
---

This site is build using a static site generator. I do not use any embedded JavaScripts, nor does this site set any cookies or create access stats, since I do not really care about reach and target advertisement. Therefore I do not process or collect any personal data as defined by GDPR.
