---
author: ann
title: "About"
date: 2018-06-20T19:42:09+02:00
---

Ann is:

  * geeky
  * poly
  * queer
  * a huffleclaw

She loves:

  * Front-End-Developement
  * iOS
  * Console Hacking
  * Tattoos

She is not:

  * yours.

As of now, you can:

  * Contact her.
  * Book her for workshops.

As of now, you cannot:

  * hire her. #sorrynotsorry
